# Práctica de la semana del día 11 de Mayo

Esta práctica consiste en preparar una aplicación para que pueda ser ejecutada y desplegada utilizando Docker.

El `package.json` del proyecto debe contener los siguientes scripts:

* `start`. Debe iniciar la ejecución de todos los componentes de la aplicación de manera local. Cada componente debe ser ejecutado en un contenedor. Es conveniente utilizar `docker-compose` para facilitar esta tarea.
* `publish`. Debe crear imágenes de cada componente de la aplicación y subirlas a un repositorio de imágenes.

El pull request para presentar este trabajo debe realizarse antes del día 25 de Mayo.
