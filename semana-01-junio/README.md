# Práctica de la semana del día 1 de junio

Esta práctica consiste en utilizar técnicas de control de calidad en una aplicación.

Utilizando [el servidor que implementa el Conway's Game of Life](https://github.com/gmunguia/demo-uneat) como servicio de backend, crear un sitio web que permita interactuar con el juego.

El sitio debe permitir configurar un estado inicial manualmente.

El sitio debe representar el estado actual de la aplicación.

El estado de la aplicación debe evalucionar periódicamente (por ejemplo, cada segundo).

El proyecto debe contener, al menos, tests de extremo a extremo y [tests unitarios](https://reactjs.org/docs/testing.html) de la interfaz visual.

El proyecto debe utilizar `docker-compose` para ejecutar el sition web y el backend al mismo tiempo. Tened en cuenta que ya existe una imagen Docker del servicio de backend.

Podéis utilizar las siguientes librerías:

* React
* [Cypress](https://www.cypress.io/)
* [React Testing Library](https://testing-library.com/docs/react-testing-library/introl)
* [Mocha](https://mochajs.org/)
* [Sinon](https://sinonjs.org/)

El pull request para presentar este trabajo debe realizarse antes del día 8 de junio.
